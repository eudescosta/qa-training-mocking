﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MockingExample
{
    // This annotation informs mstest framework that this class is a test class
    [TestClass]
    public class ConsumerTest
    {
        // this is going to be executed in the end of the each test case
        [TestCleanup]
        public void cleanup()
        {
            // code here
        }

        // this is giong to be executed in the beging of the test case
        [TestInitialize]
        public void setup()
        {
            // code here
        }

        // this is test case
        // btw, it's failing, why?
        [TestMethod]
        public void my1stMockTest()
        {
            var interfaceMock = new Mock<Interface>();
            interfaceMock.Setup(u => u.super_awesome_calculus()).Returns(50);

            var consumer = new Consumer();
            var result = consumer.Consume(interfaceMock.Object);

            Assert.AreEqual(result, 20);
        }
    }
}
