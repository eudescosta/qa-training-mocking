﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockingExample
{
    // A interface is like a "contract" every one that wants to comply with this "system" need to implement this interface, this way we assure that 
    //  the application using the interface will get what it needs.
    public interface Interface
    {
        // interface method requesting everyone to implement this method.
        int super_awesome_calculus();

        // interfaces cannot have imlplementations. If you have methods that will be similar among every one that implements it, you can have
        //  an ABSTRACT CLASS that can have a mix of method implementation and methods that needs to be immplemented for those inheriting from it.
    }
}
