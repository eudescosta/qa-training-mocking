﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockingExample
{
    // This class implements the interface class.
    class Implementation : Interface
    {
        // the interface classe requires that whoever implements it, needs to code the "super_awesome_calculus" method
        public int super_awesome_calculus()
        {
            return 10 + 10;
        }
    }
}
