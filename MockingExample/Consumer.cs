﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MockingExample
{
    // This is something that wants to consume whoever implemented the interface.
    // Note that he does not know WHAT implemented, he only know that the implementation WILL FOLLOW, whatever the interface requested.
    public class Consumer
    {
        // just an ordinary method that uses as instance of whoever implemented the interface
        public int Consume(Interface myInterface)
        {
            return myInterface.super_awesome_calculus() + 10;
        }
    }
}
